import csv
from collections import OrderedDict
from string import Template
import os
import subprocess
##from rootpy import ROOT as rt
##from rootpy.io import root_open
import emjet_limit_settings as settings
##################################################################################################
# combine tool has fitting issues when signal rate much larger than background rate.
# Scale signal rate by a constant and take into account when calculating limit on cross section
# in get_limits.py
##################################################################################################
# signal_scale  = settings.outer.signal_scale
signal_scales = settings.outer.signal_scales
# print 'signal_scale: %f' % (signal_scale)
run_blind     = settings.outer.run_blind

datacard_path = 'datacards'
lumi_integrated_fb = 16.132397 # Integrated lumi for processed G-H JetHT data

# sigfile = 'emjet_signals_20180126.csv'
# sigfile = 'emjet_signals_bestcuts_20180423.csv'
# bgfile = 'emjet_background_20180402.csv'
# nEm >= N vs nEM == N testing
# sigfile = 'emjet_signals_acc0_20180426.csv'
# bgfile = 'emjet_background_20180123_unblinded.csv'
# sigfile = 'emjet_signals_allcuts_20180504.csv'
sigfile = 'emjet_signals_combined_20180613.csv'
bgfile = 'emjet_background_20180610.csv'

# Read into list of OrderedDict
with open(sigfile) as csvfile:
    reader = csv.DictReader(csvfile)
    keys = reader.fieldnames
    r = csv.reader(csvfile)
    signal_list = [OrderedDict(zip(keys, row)) for row in r]

backgrounds = OrderedDict()
with open(bgfile) as csvfile:
    r = csv.reader(csvfile, skipinitialspace=True)
    next(r) # Ignore first line
    for row in r:
        cutname                = row[0]
        bg_predicted           = float(row[1])
        bg_predicted_stat      = float(row[2])
        bg_predicted_sys       = float(row[3])
        observed               = float(row[4])
        backgrounds[cutname]   = (bg_predicted, bg_predicted_stat, bg_predicted_sys, observed)

for i, s in enumerate(signal_list):
    signal_name                 = s['name']
    cutname                     = s['cutname']
    # print 'Processing sample %d\t: %s' % (i, signal_name)
    datacard_name               = '%s.%s.txt' % (signal_name, cutname)
    mass_X_d                    = float(s['mass_X_d'])
    if mass_X_d != 2000: continue
    mass_pi_d                   = float(s['mass_pi_d'])
    tau_pi_d                    = float(s['tau_pi_d'])
    acceptance                  = float(s['acceptance'])
    xsec_fb                     = float(s['xsec_fb'])
    acceptance                  = float(s['acceptance'])
    # acceptance_shift_PileupUp   = float(s['acceptance_shift_PileupUp'])
    # acceptance_shift_PileupDn   = float(s['acceptance_shift_PileupDn'])
    acceptance_shift_PileupUp   = 0. # FIXME
    acceptance_shift_PileupDn   = 0. # FIXME
    acceptance_shift_PdfUp      = float(s['acceptance_shift_PdfUp'])
    acceptance_shift_PdfDn      = float(s['acceptance_shift_PdfDn'])
    # acceptance_shift_ModelingUp = float(s['acceptance_shift_ModelingUp'])
    acceptance_shift_ModelingUp = 0. # FIXME
    signal_scale = signal_scales[mass_X_d]
    # print acceptance
    # print s['acceptance_shift_ModelingUp']
    # print acceptance_shift_ModelingUp
    acceptance_shift_TriggerUp  = float(s['acceptance_shift_TriggerUp'])
    acceptance_shift_MCStatUp  = float(s['acceptance_shift_MCStatUp'])
    # Calculate fields
    if acceptance != 0:
        ratio_PileupUp              = 1. + acceptance_shift_PileupUp / acceptance
        ratio_PileupDn              = 1. + acceptance_shift_PileupDn / acceptance
        ratio_Pileup                = 1. + abs(acceptance_shift_PileupDn) / acceptance # FIXME Temporary
        ratio_PdfUp                 = 1. + acceptance_shift_PdfUp / acceptance
        ratio_PdfDn                 = 1. + acceptance_shift_PdfDn / acceptance
        ratio_ModelingUp            = 1. + acceptance_shift_ModelingUp / acceptance
        ratio_TriggerUp             = 1. + acceptance_shift_TriggerUp / acceptance
        ratio_MCStatUp              = 1. + acceptance_shift_MCStatUp / acceptance
    else:
        ratio_PileupUp              = 1.
        ratio_PileupDn              = 1.
        ratio_Pileup                = 1.
        ratio_PdfUp                 = 1.
        ratio_PdfDn                 = 1.
        ratio_ModelingUp            = 1.
        ratio_TriggerUp             = 1.
        ratio_MCStatUp              = 1.
    # print ratio_ModelingUp
    # print signal_name, mass_X_d, mass_pi_d, tau_pi_d, acceptance, xsec_fb, acceptance, acceptance_shift_PdfUp, acceptance_shift_PdfDn, acceptance_shift_ModelingUp, ratio_PdfUp, ratio_PdfDn, ratio_ModelingUp
    # Get predicted and observed number of events
    bg_predicted                = backgrounds[cutname][0]
    bg_predicted_stat           = backgrounds[cutname][1]
    bg_predicted_sys            = backgrounds[cutname][2]
    observed                    = backgrounds[cutname][3]
    ratio_bg_predicted_sys         = 1. + bg_predicted_sys / bg_predicted
    ratio_bg_predicted_stat        = 1. + bg_predicted_stat / bg_predicted

    # Fill in fields in datacard
    kwdict={}
    # Observed events
    kwdict['obs']      = '%d' % observed
    # Predicted rate
    kwdict['rate_S']   = '%f' % (xsec_fb * lumi_integrated_fb * acceptance*signal_scale)
    # kwdict['rate_S']   = '%f' % (xsec_fb * lumi_integrated_fb * acceptance)
    # print signal_name, kwdict['rate_S']
    kwdict['rate_B']   = '%f' % bg_predicted
    # Systematic uncertainties
    # - Luminosity - affects signal
    # - Cross section uncertainty - affects signal
    # - PDF uncertainty - affects signal
    # - Factorization/Renormalization scale uncertainty - affects signal
    # - Jet variable modeling uncertainty - affects signal and QCD
    # - Data-driven background estimate uncertainty - affects QCD
    kwdict['lumi_S']         = '%f' % 1.025
    kwdict['xs_S']           = '%f' % 1.00
    kwdict['pileup_S_up']    = '%f' % ratio_PileupUp
    kwdict['pileup_S_dn']    = '%f' % ratio_PileupDn
    kwdict['pdf_S_up']       = '%f' % ratio_PdfUp
    kwdict['pdf_S_dn']       = '%f' % ratio_PdfDn
    kwdict['scale_S']        = '%f' % 1.00
    kwdict['hlt_S']          = '%f' % ratio_TriggerUp
    kwdict['jetvar_S']       = '%f' % ratio_ModelingUp
    kwdict['jetvar_B']       = '%f' % 1.00
    kwdict['dde_sys_B']      = '%f' % ratio_bg_predicted_sys
    kwdict['dde_stat_B']     = '%f' % ratio_bg_predicted_stat
    kwdict['mcstat_S']       = '%f' % ratio_MCStatUp

    datacard_template = open('template-emjet-realistic-counting-experiment-fake-shape.txt', 'r')
    cwd = os.getcwd()
    # datacard_path = os.path.join(cwd, '', datacard_name)
    datacard_file = open (os.path.join(datacard_path, datacard_name), 'w')
    for line in datacard_template:
        t = Template(line)
        subline = t.substitute(kwdict)
        # print subline.rstrip('\n')
        datacard_file.write(subline)
    datacard_file.close()
    datacard_template.close()

# Submit condor jobs
doSubmit = 1
if doSubmit:
    for s in signal_list:
        signal_name                 = s['name']
        cutname                     = s['cutname']
        datacard_name               = '%s.%s.txt' % (signal_name, cutname)
        datacard_file = os.path.join(datacard_path, datacard_name)
        # Run combine tool
        combine_options = ""
        if run_blind: combine_options += "--run blind "
        # combine_command = 'combine -M Asymptotic %s -n .%s %s' % (combine_options, signal_name, datacard_file)
        combine_command = './combine_singlejob.py \"-M Asymptotic %s -n .%s.%s %s\"' % (combine_options, signal_name, cutname, datacard_file)
        #subprocess.call(combine_command, shell=True)
        p=subprocess.Popen(combine_command, shell=True)
        p.wait()

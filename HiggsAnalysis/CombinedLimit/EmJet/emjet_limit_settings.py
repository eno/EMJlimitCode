"""File to hold limit-related settings used in both run_limits.py and get_limits.py"""

class outer:
    pass

# The Higgs Combine tool doesn't give sensible results when signal is much lower than background.
# A simple workaround is to scale the signal rate up a fixed constant, "signal_scale",
# then scale the resulting signal strength down by the same constant
outer.signal_scale = 1000
# Run blinded limits, i.e. skip observed limits
outer.run_blind = False

outer.signal_scales = {}
# outer.signal_scales[400]  = 1000
# outer.signal_scales[600]  = 1000
# outer.signal_scales[800]  = 100
# outer.signal_scales[1000] = 10
# outer.signal_scales[1250] = 10
outer.signal_scales[400]  = 1
outer.signal_scales[600]  = 1
outer.signal_scales[800]  = 1
outer.signal_scales[1000] = 1
outer.signal_scales[1250] = 1
outer.signal_scales[1500] = 1
outer.signal_scales[2000] = 1
#outer.signal_scales[2000] = 10 # for mass_X_d_2000_mass_pi_d_1_tau_pi_d_1.cut5 only


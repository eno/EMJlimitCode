from string import Template
import os

datacard_path = '.'
datacard_name = 'test.txt'

kwdict={}
# Observed events
kwdict['obs']      = '%d' % 40
# Predicted rate
kwdict['rate_S']   = '%f' % 57.35
kwdict['rate_B']   = '%f' % 23.21
# Systematic uncertainties
# - Luminosity - affects signal
# - Cross section uncertainty - affects signal
# - PDF uncertainty - affects signal
# - Factorization/Renormalization scale uncertainty - affects signal
# - Jet variable modeling uncertainty - affects signal and QCD
# - Data-driven background estimate uncertainty - affects QCD
kwdict['lumi_S']   = '%f' % 1.025
kwdict['xs_S']     = '%f' % 1.00
kwdict['pdf_S']    = '%f' % 1.05
kwdict['scale_S']  = '%f' % 1.05
kwdict['jetvar_S'] = '%f' % 1.35
kwdict['jetvar_B'] = '%f' % 1.30
kwdict['dde_B']    = '%f' % 1.35

datacard_template = open('template-emjet-realistic-counting-experiment.txt', 'r')
# datacard_path = os.path.join(cwd, jobdirname, jobname, genfragname)
cwd = os.getcwd()
datacard_path = os.path.join(cwd, datacard_name)
datacard_file = open (datacard_name, 'w')
for line in datacard_template:
    t = Template(line)
    subline = t.substitute(kwdict)
    # print subline.rstrip('\n')
    datacard_file.write(subline)
datacard_file.close()
datacard_template.close()

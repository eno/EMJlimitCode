import csv
from collections import OrderedDict
from string import Template
import os
import subprocess
from rootpy import ROOT as rt
from rootpy.io import root_open
import emjet_limit_settings as settings
# signal_scale = settings.outer.signal_scale
signal_scales = settings.outer.signal_scales
run_blind    = settings.outer.run_blind

datacard_path = '.'
# Integrated lumi for processed G-H JetHT data
lumi_integrated_fb = 16.132397

# sigfile = 'emjet_signals_allcuts_20180323.csv'
# sigfile = 'emjet_signals_acc1_20180426.csv'
# sigfile = 'emjet_signals_allcuts_20180504.csv'
# sigfile = 'emjet_signals_bestcuts_20180509_combined.csv'
sigfile = 'emjet_signals_combined_20180613.csv'
output_limits_file = 'emjet_limits_20180613_combined_p20190108.csv'

# Read into list of OrderedDict
with open(sigfile) as csvfile:
    reader = csv.DictReader(csvfile)
    keys = reader.fieldnames
    r = csv.reader(csvfile)
    signal_list = [OrderedDict(zip(keys, row)) for row in r]

limit_table = []
for s in signal_list:
    signal_name                 = s['name']
    cutname                     = s['cutname']
    datacard_name               = '%s.txt' % signal_name
    limitfilename = 'higgsCombine.%s.%s.Asymptotic.mH120.root' % (signal_name, cutname)
    limitfileh = root_open(limitfilename)
    limits = []
    limit_errors = []
    mass_X_d = float(s['mass_X_d'])
    signal_scale = signal_scales[mass_X_d]
    try:
        for i, entry in enumerate(limitfileh.limit):
            # print i, entry.limit, entry.quantileExpected
            limits.append(entry.limit)
            limit_errors.append(entry.limitErr)
        if len(limits) < 5:
            print "Limits missing in file: %s" % limitfilename
            limits = [-1] * 6
            limit_errors = [-1] * 6
    except AttributeError:
        continue
    xsec_fb = float(s['xsec_fb'])
    limit_table_entry = OrderedDict()
    limit_table_entry['name']       = s['name']
    limit_table_entry['cutname']    = s['cutname']
    limit_table_entry['mass_X_d']   = float(s['mass_X_d'])
    limit_table_entry['mass_pi_d']  = float(s['mass_pi_d'])
    limit_table_entry['tau_pi_d']   = float(s['tau_pi_d'])
    limit_table_entry['acceptance'] = float(s['acceptance'])
    limit_table_entry['xsec_fb']    = float(s['xsec_fb'])
    limit_table_entry['sigma-2']    = limits[0] * xsec_fb * signal_scale
    limit_table_entry['sigma-1']    = limits[1] * xsec_fb * signal_scale
    limit_table_entry['sigma0']     = limits[2] * xsec_fb * signal_scale
    limit_table_entry['sigma1']     = limits[3] * xsec_fb * signal_scale
    limit_table_entry['sigma2']     = limits[4] * xsec_fb * signal_scale
    # limit_table_entry['sigma-2']    = limits[0] * xsec_fb
    # limit_table_entry['sigma-1']    = limits[1] * xsec_fb
    # limit_table_entry['sigma0']     = limits[2] * xsec_fb
    # limit_table_entry['sigma1']     = limits[3] * xsec_fb
    # limit_table_entry['sigma2']     = limits[4] * xsec_fb
    if run_blind:
        limit_table_entry['observed']   = -1
        limit_table_entry['observed_err']   = -1
    else:
        # limit_table_entry['observed']   = limits[5] * xsec_fb / signal_scale
        limit_table_entry['observed']   = limits[5] * xsec_fb
        limit_table_entry['observed_err']   = limit_errors[5] * xsec_fb
    limit_table.append(limit_table_entry)
    limitfileh.Close()

if 1:
    # Write results to CSV file
    # output_limits_file = 'emjet_limits_allcuts_20180509.csv'
    with open(output_limits_file, 'w') as csvfile:
        # csvfile.write("sigma-2, sigma-1, sigma0, sigma1, sigma2, observed\n")
        writer = csv.DictWriter(csvfile, fieldnames=limit_table[0].keys())
        writer.writeheader()
        for s in limit_table:
            writer.writerow(s)

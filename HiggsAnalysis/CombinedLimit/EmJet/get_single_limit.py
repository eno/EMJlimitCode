from rootpy import ROOT as rt
from rootpy.io import root_open
import argparse

parser = argparse.ArgumentParser()
# parser.add_argument('-f', '--force', action='store_true')
parser.add_argument('limitfilename', type=str)
args = parser.parse_args()

limitfile = root_open(args.limitfilename, 'read')

# Simple counting experiment, with one signal and a few background processes 
# Simplified version of the 35/pb H->WW analysis for mH = 160 GeV
imax 1   number of channels
jmax 1   number of backgrounds
kmax 10  number of nuisance parameters (sources of systematical uncertainties)
------------
# Convert cut-and-count to equivalent (fake) shape analysis - Gives better fitting results
shapes * * FAKE
------------
# we have just one channel, in which we observe 0 events
bin 1
observation 14
------------
# now we list the expected events for signal and all backgrounds in that bin
# the second 'process' line must have a positive number for backgrounds, and 0 for signal
# then we list the independent sources of uncertainties, and give their effect (syst. error)
# on each process and bin
bin                1     1   
process          emjet QCD  
process            0     1   
rate             88.411287  14.090000
------------
# Systematic uncertainties
# - Luminosity - affects signal
# - Cross section uncertainty - affects signal
# - PDF uncertainty - affects signal
# - Factorization/Renormalization scale uncertainty - affects signal
# - HLT trigger uncertainty - affects signal
# - Jet variable modeling uncertainty - affects signal and QCD
# - Data-driven background estimate uncertainty - affects QCD
lumi       lnN    1.025000    -   
xs_emjet   lnN    1.000000    -   
pileup     lnN    1.000000/1.000000    -   
pdf        lnN    0.998461/1.002277    -   
scale      lnN    1.000000    -
hlt        lnN    0.911995    -   
jetvar     lnN    1.000000  1.000000
dde_qcd    lnN      -   1.039744
dde_stat   lnN      -   1.136977
mcstat     lnN    1.028047    -
